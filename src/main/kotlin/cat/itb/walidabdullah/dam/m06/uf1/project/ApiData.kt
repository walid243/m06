package cat.itb.walidabdullah.dam.m06.uf1.project

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

class ApiData {
    private lateinit var data : BusData
    val client =  HttpClient(CIO) {
            install(ContentNegotiation) {
                json(Json {
                    ignoreUnknownKeys = true
                })
            }

    }


    suspend fun getPrediction(parada: String, linea: String): BusData {
        val url = "https://api.tmb.cat/v1/ibus/lines/${linea}/stops/${parada}?app_id=d7930e37&app_key=29f4414676743490e4901935b1fab8bc"
        return client.get(url).body()
    }
}