package cat.itb.walidabdullah.dam.m06.uf2.exercicis.mongodb.model

//import io.realm.kotlin.types.RealmObject
//import io.realm.kotlin.types.annotations.PrimaryKey
//import org.bson.types.ObjectId
//
//open class Item(
//    @PrimaryKey
//    var _id: ObjectId = ObjectId.get(),
//    var complete: Boolean = false,
//    var summary: String = "",
//    var owner_id: String = ""
//) : RealmObject {
//    // Declaring empty contructor
//    constructor() : this(owner_id = "") {}
//    //var doAfter: RealmList<Item>? = realmListOf()
//    override fun toString() = "Item($_id, $summary)"
//}