package cat.itb.walidabdullah.dam.m06.uf2.exercicis.mongodb

import cat.itb.walidabdullah.dam.m06.uf2.exercicis.mongodb.model.Sport
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.log.LogLevel
import io.realm.kotlin.mongodb.*
import io.realm.kotlin.mongodb.sync.SyncConfiguration

object RealmApp {
    val app: App = App.create(
       AppConfiguration.Builder("sporttrakerrealmsync-wfubt")
           .log(LogLevel.ALL)
           .build()
   )
    lateinit var currentUser : User
    lateinit var realm : Realm

    suspend fun login(userName: String, password: String) {
        val credentials = Credentials.emailPassword(userName,password)
        app.login(credentials)
        currentUser = app.currentUser!!
        openRealm(config())
        subscriptRealm()
    }

    suspend fun register(userName: String, password: String) {
        app.emailPasswordAuth.registerUser(userName, password)
        login(userName, password)
    }

    fun config(): SyncConfiguration {
        return SyncConfiguration.Builder(currentUser, setOf(Sport::class))
            .initialSubscriptions{realm ->
                add(
                    realm.query<Sport>(), "All Sports"
                )
            }
//            .waitForInitialRemoteData()
            .build()

    }

    fun openRealm(config: SyncConfiguration){
        realm = Realm.open(config)
    }

    suspend fun subscriptRealm(){
        realm.subscriptions.waitForSynchronization()
    }

    fun insert(name: String, time: Int) {
        val sport = Sport(name = name, time = time, ownerId = currentUser.id)
        realm.writeBlocking {
            copyToRealm(sport)
        }
    }
}