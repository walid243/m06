package cat.itb.walidabdullah.dam.m06.uf1.repas

import java.io.File
import kotlin.io.path.Path
import kotlin.io.path.forEachDirectoryEntry
import kotlin.io.path.writeText

fun main() {
    val desktop = Path("/home/sjo/Escriptori").toFile()
    Path(System.getProperty("user.home"),"desktopContents.txt").writeText(desktop.walk().maxDepth(1).count().toString())

}