package cat.itb.walidabdullah.dam.m06.uf1.project

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Predict(val routeId:String, @SerialName("t-in-min") val t_in_min:Int, @SerialName("t-in-s") val t_in_s:Int, @SerialName("text-ca") val text_predict: String, val destination:String) {

}

@Serializable
class Lines()

@Serializable
data class Data(@SerialName("ibus") val busData: List<Predict>) {}

@Serializable
data class BusData(val status: String, val data: Data)