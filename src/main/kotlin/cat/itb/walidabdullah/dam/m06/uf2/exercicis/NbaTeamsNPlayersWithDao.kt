package cat.itb.walidabdullah.dam.m06.uf2.exercicis

import cat.itb.walidabdullah.dam.m06.uf2.exercicis.Players.varchar
import cat.itb.walidabdullah.dam.m06.uf2.exercicis.Teams
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Table

class Team(name: String): Entity<String>(Teams.name as EntityID<String>){
    companion object : EntityClass<String, Team>(Teams as IdTable<String>)
    val name by Teams.name
    val city by Teams.city
    val conference by Teams.conference
    val division by Teams.division
}
//class Player(id) {
//    val id
//    val name
//    val origin
//    val height
//    val weight
//    val position
//    val nameIteam
//}

fun main() {

}
