package cat.itb.walidabdullah.dam.m06.uf1.project

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.io.path.*

class DataStorage {
    var savedStops = mutableMapOf<String, String>()
    fun save(stop: String, stopName:String){
        if (stopName != null && stop != null){
            savedStops[stopName!!] = stop!!
        }
        Path("data/savedStops.json").writeText(Json.encodeToString(savedStops))
    }

    fun getStop(stop: String) =
        if (savedStops.containsKey(stop))
            savedStops[stop]
        else null


 fun createIfNotExists(){
     if (Path("data/savedStops.json").exists()){
         savedStops = Json.decodeFromString(Path("data/savedStops.json").readText())
     } else {
         if (!Path("data").isDirectory()){
             Path("data").createDirectory()
         }
     }
     println(savedStops)
 }
}