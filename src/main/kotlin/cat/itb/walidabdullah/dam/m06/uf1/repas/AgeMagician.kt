package cat.itb.walidabdullah.dam.m06.uf1.repas

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.util.Scanner


@Serializable
class AgeMagician(val name:String){
    var age: Int = 0
    var count:Int = 0
    var country_id:String = ""
}
suspend fun main() {
    val client = HttpClient(CIO){
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val sc = Scanner(System.`in`)
    println("Ben vingut al AgeMagician. Com et dius?")
//    val age: AgeMagician  = client.get<AgeMagician>("https://api.agify.io/?name=${sc.next()}")

}