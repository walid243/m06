package cat.itb.walidabdullah.dam.m06.uf2.exercicis.mongodb

import cat.itb.walidabdullah.dam.m06.uf2.exercicis.mongodb.model.Sport
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.ext.query
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*

suspend fun main() {
    runBlocking {
        val sc = Scanner(System.`in`)
        val config = RealmConfiguration.Builder(setOf(Sport::class))
            .deleteRealmIfMigrationNeeded()
            .directory("data")
            .build()
        println("Realm Path: ${config.path}")
        val realm = Realm.open(config)
        launch {
            realm.query<Sport>().find().asFlow().collect {
                val sports = it.list
                sports.forEach(::println)

                println("Total time: ${realm.query<Sport>().sum("time", Int::class).find()}")

                println("Total time for sport: ")
                realm.query<Sport>().distinct("name").find().forEach{sport ->
                    println("${sport.name}: ${realm.query<Sport>("name == $0",sport.name).sum("time", Int::class).find()}")
                }
            }
        }
        launch {
            realm.writeBlocking {
                val sport = Sport().apply {
                    name = sc.next()
                    time = sc.nextInt()
                    // doAfter = query<Item>().find().take(2).toRealmList()
                }
                copyToRealm(sport)
            }
            realm.close()
        }

    }
}