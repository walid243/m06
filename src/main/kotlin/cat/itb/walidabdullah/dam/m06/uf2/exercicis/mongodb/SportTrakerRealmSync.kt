package cat.itb.walidabdullah.dam.m06.uf2.exercicis.mongodb

import java.util.*

suspend fun main() {
    val sc = Scanner(System.`in`)
    println("1: Login\n2: Register")
    when(sc.nextInt()){
        1 -> login(takeData(sc))
        2 -> register(takeData(sc))
    }

    println("Welcome ${RealmApp.currentUser}, do you have something new to add?")
    println("1: Yes\n2: No")
    when(sc.nextInt()) {
        1 -> addData(sc)
        2 -> println("Bye!")
    }


}

fun addData(sc: Scanner) {
    println("Sport: ")
    val sport = sc.next()
    println("Time: ")
    val time = sc.nextInt()
    RealmApp.insert(sport, time)
}

suspend fun register(userData: Pair<String, String>) {
    RealmApp.register(userData.first,userData.second)
}

suspend fun login(userData: Pair<String, String>) {
    RealmApp.login(userData.first, userData.second)
}

fun takeData(sc: Scanner): Pair<String, String> {
    println("userName:")
    val userName = sc.next()
    println("password")
    val password = sc.next()
    return Pair(userName, password)
}