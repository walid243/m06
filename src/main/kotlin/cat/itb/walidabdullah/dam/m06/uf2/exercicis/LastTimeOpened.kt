package cat.itb.walidabdullah.dam.m06.uf2.exercicis

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.text.SimpleDateFormat
import java.util.*

fun main() {
    //an example connection to H2 DB
    Database.connect("jdbc:h2:./myh2file", "org.h2.Driver")

    transaction {
        addLogger(StdOutSqlLogger)

        SchemaUtils.create (Registry)
        Registry.insert {
            it[date] = SimpleDateFormat("dd/M/yyyy hh:mm:ss").format(Date())
        }

        // 'select *' SQL: SELECT Cities.id, Cities.name FROM Cities
        Registry.selectAll().forEach{ println("Date:  ${it[Registry.date]}") }
    }
}

object Registry : Table() {
    val registerId: Column<Int> = integer("id").autoIncrement()
    val date: Column<String> = varchar("date", 50)
}