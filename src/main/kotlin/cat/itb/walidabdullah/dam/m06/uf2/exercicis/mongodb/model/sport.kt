package cat.itb.walidabdullah.dam.m06.uf2.exercicis.mongodb.model

import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey
import org.bson.types.ObjectId

open class Sport(
    @PrimaryKey
    var _id: ObjectId = ObjectId.get(),
    var name: String,
    var time: Int,
    var ownerId: String
): RealmObject {
    // Declaring empty contructor
    constructor() : this(name = "", time = 0, ownerId = "")

        var doAfter: RealmList<Sport>? = realmListOf()
    override fun toString() = "Sport($$name, $time)"
}