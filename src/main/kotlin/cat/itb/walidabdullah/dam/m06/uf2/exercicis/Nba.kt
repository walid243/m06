package cat.itb.walidabdullah.dam.m06.uf2.exercicis

import cat.itb.walidabdullah.dam.m06.uf2.exercicis.Teams.name
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)
    val db = Database.connect(
        "jdbc:postgresql://localhost:5432/nba", driver = "org.postgresql.Driver",
        user = "sjo"
    )
    transaction{
        Teams.selectAll().forEach { println(it[Teams.name]) }
    }
    println("Que jugador quieres?")
    val playerName = sc.nextLine()
    transaction {
        Teams.innerJoin(Players).select( where = {name eq Players.nameIteam and (Players.name eq playerName)}).forEach{ println(it[Teams.city]) }
    }
// select teams.name, Max(players.height) from players inner join teams on players.nameiteam = teams.name where teams.division = 'Atlantic' group by teams.name;
    transaction {
        (Players innerJoin Teams)
            .slice(Teams.name, Players.height.max())
            .select(Teams.division eq "Atlantic")
            .groupBy(Teams.name)
            .forEach { println("${it[Teams.name]}, ${it[Players.height.max()]}") }
    }
    transaction {
        val data = mutableListOf<Double>()
        var weight = 0.0
        var height = 0.0
        var imc = 0.0
        Players.slice(Players.weight, Players.height)
            .selectAll()
            .forEach{
                weight = it[Players.weight].toDouble()
                height = it[Players.height].replace(',','.').toDouble()
                imc = weight/(height * height)
                data.add(imc)
            }
        println(data.average())
    }

}
object Teams: Table("Teams") {
   val name = varchar("name",20).uniqueIndex()
   val city =  varchar("city",20)
   val conference = varchar("conference",4)
   val division = varchar("division",9)
}
object Players: Table("Players") {
    val id =  integer("id").uniqueIndex()
    val name = varchar("name",30)
    val origin = varchar("origin",20)
    val height = varchar("height",4)
    val weight = integer("weight")
    val position = varchar("position",12)
    val nameIteam = reference("nameiteam", Teams.name)
}